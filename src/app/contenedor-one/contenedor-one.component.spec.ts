import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorOneComponent } from './contenedor-one.component';

describe('ContenedorOneComponent', () => {
  let component: ContenedorOneComponent;
  let fixture: ComponentFixture<ContenedorOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorOneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedorOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
