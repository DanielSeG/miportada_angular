import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorThreeComponent } from './contenedor-three.component';

describe('ContenedorThreeComponent', () => {
  let component: ContenedorThreeComponent;
  let fixture: ComponentFixture<ContenedorThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorThreeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedorThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
