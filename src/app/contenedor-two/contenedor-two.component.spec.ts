import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorTwoComponent } from './contenedor-two.component';

describe('ContenedorTwoComponent', () => {
  let component: ContenedorTwoComponent;
  let fixture: ComponentFixture<ContenedorTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorTwoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedorTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
