import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ContenedorOneComponent } from './contenedor-one/contenedor-one.component';
import { ContenedorTwoComponent } from './contenedor-two/contenedor-two.component';
import { ContenedorThreeComponent } from './contenedor-three/contenedor-three.component';
import { PruNGComponent } from './pru-ng/pru-ng.component';

@NgModule({
  declarations: [
    AppComponent,
    ContenedorOneComponent,
    ContenedorTwoComponent,
    ContenedorThreeComponent,
    PruNGComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
